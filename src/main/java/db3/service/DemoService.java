package db3.service;

import db3.entity.Item;
import db3.entity.User;
import db3.repository.IBidsRepo;
import db3.repository.IItemsRepo;
import db3.repository.IUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DemoService implements IDemoService {

    @Autowired
    IBidsRepo bidsRepo;

    @Autowired
    IItemsRepo itemsRepo;

    @Autowired
    IUserRepo userRepo;

    @Override
    public void execute() {

        //1
        System.out.println("1 запрос");
        bidsRepo.getByUserID(5).forEach(System.out::println);
        System.out.println();

        //2
        System.out.println("2 запрос");
        itemsRepo.getItemsByID(1).forEach(System.out::println);
        System.out.println();

        //3
        System.out.println("3 запрос");
        itemsRepo.findAndGetItemsByName("KIA").forEach(System.out::println);
        System.out.println();

        //4
        System.out.println("4 запрос");
        itemsRepo.findAndGetItemsByDescription("битый").forEach(System.out::println);
        System.out.println();

        //5
        System.out.println("5 запрос");
        itemsRepo.avgPriceForEachUser().forEach(System.out::println);
        System.out.println();

        //6
        System.out.println("6 запрос");
        bidsRepo.maxBidsForEachLots().forEach(System.out::println);
        System.out.println();

        //7
        System.out.println("7 запрос");
        itemsRepo.activeLotsForThisUser(3).forEach(System.out::println);
        System.out.println();

        //8
        System.out.println("8 запрос");
        userRepo.addNewUser(new User(12, "Евгений Евгеньев",
                "ул.Карла Маркса, 12-228, Россия, Кировская область, Киров",
                "evgesshha", "dagwrhb442"));
        System.out.println();

        //9
        System.out.println("9 запрос");
        itemsRepo.addNewItem(new Item(12, "Lada Priora", "Белый, без повреждений, 2015",
                60000.0, 5000.0, LocalDate.of(2019,01,20),
                LocalDate.of(2019,04,20), 10));
        System.out.println();

        //10
        System.out.println("10 запрос");
        bidsRepo.deleteBidsByUserId(2);
        System.out.println();

        //11
        System.out.println("11 запрос");
        itemsRepo.deleteItemsByUserId(6);
        System.out.println();

        //12
        System.out.println("12 запрос");
        itemsRepo.setDoubleItemPriceByUserId(1);
        System.out.println();

        //13
        System.out.println("13 запрос");
        itemsRepo.setDoublePriceIfCarIsRed();
    }
}
