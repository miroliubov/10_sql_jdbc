package db3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackages = {"db3.service", "db3.repository"})
public class AppConfig {

    @Bean
    public JdbcTemplate jdbcTemplate () {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/internet_auction?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Europe/Samara");
        dataSource.setUsername("root");
        dataSource.setPassword("qazwsx");
        return new JdbcTemplate(dataSource);
    }
}
