package db3.entity;

public class Bid {

    String id;
    String bidDate;
    String bidValue;
    String userID;
    String itemID;

    public Bid(String bid_id, String bid_date, String bid_value, String users_user_id, String items_item_id) {
        this.id = bid_id;
        this.bidDate = bid_date;
        this.bidValue = bid_value;
        this.userID = users_user_id;
        this.itemID = items_item_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBidDate() {
        return bidDate;
    }

    public void setBidDate(String bidDate) {
        this.bidDate = bidDate;
    }

    public String getBidValue() {
        return bidValue;
    }

    public void setBidValue(String bidValue) {
        this.bidValue = bidValue;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "id='" + id + '\'' +
                ", bidDate='" + bidDate + '\'' +
                ", bidValue='" + bidValue + '\'' +
                ", userID='" + userID + '\'' +
                ", itemID='" + itemID + '\'' +
                '}';
    }
}
