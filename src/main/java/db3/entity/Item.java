package db3.entity;

import java.time.LocalDate;

public class Item {

    private int item_id;
    private String title;
    private String description;
    private double start_price;
    private double bid_increment;
    private LocalDate start_date;
    private LocalDate stop_date;
    private int users_user_id;


    public Item(int item_id, String title, String description,
                double start_price, double bid_increment,
                LocalDate start_date, LocalDate stop_date, int users_user_id) {
        this.item_id = item_id;
        this.title = title;
        this.description = description;
        this.start_price = start_price;
        this.bid_increment = bid_increment;
        this.start_date = start_date;
        this.stop_date = stop_date;
        this.users_user_id = users_user_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getStart_price() {
        return start_price;
    }

    public void setStart_price(double start_price) {
        this.start_price = start_price;
    }

    public double getBid_increment() {
        return bid_increment;
    }

    public void setBid_increment(double bid_increment) {
        this.bid_increment = bid_increment;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDate start_date) {
        this.start_date = start_date;
    }

    public LocalDate getStop_date() {
        return stop_date;
    }

    public void setStop_date(LocalDate stop_date) {
        this.stop_date = stop_date;
    }

    public int getUsers_user_id() {
        return users_user_id;
    }

    public void setUsers_user_id(int users_user_id) {
        this.users_user_id = users_user_id;
    }

    @Override
    public String toString() {
        return "Item{" +
                "item_id=" + item_id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", start_price=" + start_price +
                ", bid_increment=" + bid_increment +
                ", start_date=" + start_date +
                ", stop_date=" + stop_date +
                ", users_user_id=" + users_user_id +
                '}';
    }
}
