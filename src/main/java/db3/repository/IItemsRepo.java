package db3.repository;

import db3.entity.Item;

import java.util.List;
import java.util.Map;

public interface IItemsRepo {
    List<Item> getItemsByID(int userId);
    List<Item> findAndGetItemsByName(String s);
    List<Item> findAndGetItemsByDescription(String s);
    List<Map<String, Object>> avgPriceForEachUser();
    List<Item> activeLotsForThisUser(int userId);
    void addNewItem(Item item);
    void deleteItemsByUserId(int userId);
    void setDoubleItemPriceByUserId(int userId);
    void setDoublePriceIfCarIsRed();
}
