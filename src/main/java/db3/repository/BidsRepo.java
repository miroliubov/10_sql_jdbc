package db3.repository;

import db3.entity.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;


@Repository
public class BidsRepo implements IBidsRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<Bid> ROW_MAPPER = (resultSet, rowNumber) ->
            new Bid(
                    resultSet.getString("bid_id"),
                    resultSet.getString("bid_date"),
                    resultSet.getString("bid_value"),
                    resultSet.getString("users_user_id"),
                    resultSet.getString("items_item_id"));

    @Override
    public List<Bid> getByUserID(int id) {
        String sql = "SELECT * FROM internet_auction.bids WHERE users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{id}, ROW_MAPPER);
    }

    @Override
    public List<Map<String, Object>> maxBidsForEachLots() {
        String sql = "SELECT title, MAX(bid_value) " +
                "FROM internet_auction.bids, internet_auction.items " +
                "WHERE items.item_id = bids.items_item_id " +
                "GROUP BY title";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public void deleteBidsByUserId(int userId) {
        String sql = "DELETE FROM internet_auction.bids\n" +
                "WHERE users_user_id = ?;";
        jdbcTemplate.update(sql, userId);
    }

}