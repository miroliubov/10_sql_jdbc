package db3.repository;

import db3.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Map;

@Repository
public class ItemsRepo implements IItemsRepo{

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<Item> ROW_MAPPER = (resultSet, rowNumber) ->
            new Item(
                    resultSet.getInt("item_id"),
                    resultSet.getString("title"),
                    resultSet.getString("description"),
                    resultSet.getDouble("start_price"),
                    resultSet.getDouble("bid_increment"),
                    resultSet.getDate("start_date").toLocalDate(),
                    resultSet.getDate("stop_date").toLocalDate(),
                    resultSet.getInt("users_user_id"));

    @Override
    public List<Item> getItemsByID(int userId) {
        String sql = "SELECT * FROM internet_auction.items " +
                "WHERE users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{userId}, ROW_MAPPER);
    }

    @Override
    public List<Item> findAndGetItemsByName(String s) {
        String sql = "SELECT * FROM internet_auction.items " +
                "WHERE title LIKE ?";
        return jdbcTemplate.query(sql, new Object[] {"%" + s + "%"}, ROW_MAPPER);
    }

    @Override
    public List<Item> findAndGetItemsByDescription(String s) {
        String sql = "SELECT * FROM internet_auction.items " +
                "WHERE description LIKE ?";
        return jdbcTemplate.query(sql, new Object[] {"%" + s + "%"}, ROW_MAPPER);
    }

    @Override
    public List<Map<String, Object>> avgPriceForEachUser() {
        String sql = "SELECT full_name, AVG(start_price)" +
                " FROM internet_auction.items, internet_auction.users" +
                " WHERE items.users_user_id = users.user_id" +
                " GROUP BY full_name";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Item> activeLotsForThisUser(int userId) {
        String sql = "SELECT * FROM internet_auction.items\n" +
                "WHERE users_user_id = ? " +
                "AND CURRENT_DATE() BETWEEN start_date AND stop_date";
        return jdbcTemplate.query(sql, new Object[]{userId}, ROW_MAPPER);
    }

    @Override
    public void addNewItem(Item item) {
        String sql = "INSERT INTO internet_auction.items " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, NULL, ?)";
        jdbcTemplate.update(sql, item.getItem_id(), item.getTitle(), item.getDescription(), item.getStart_price(),
                item.getBid_increment(), Date.valueOf(item.getStart_date()), Date.valueOf(item.getStop_date()),
                item.getUsers_user_id());
    }

    @Override
    public void deleteItemsByUserId(int userId) {
        String sql = "DELETE bid " +
                "FROM internet_auction.bids AS bid " +
                "INNER JOIN internet_auction.items AS item ON bid.items_item_id = item.item_id " +
                "WHERE item.users_user_id = ?";
        String sql2 = "DELETE FROM internet_auction.items " +
                "WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
        jdbcTemplate.update(sql2, userId);
    }

    @Override
    public void setDoubleItemPriceByUserId(int userId) {
        String sql = "UPDATE internet_auction.items " +
                "SET start_price = start_price*2 " +
                "WHERE users_user_id = ?;";
        jdbcTemplate.update(sql, userId);
    }

    @Override
    public void setDoublePriceIfCarIsRed() {
        String sql = "UPDATE internet_auction.items " +
                "SET start_price = start_price*2 " +
                "WHERE internet_auction.items.description LIKE '%Красный%'";
        jdbcTemplate.update(sql);
    }


}
