package db3.repository;

import db3.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepo implements IUserRepo {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<User> ROW_MAPPER = (resultSet, rowNumber) ->
            new User(
                    resultSet.getInt("user_id"),
                    resultSet.getString("full_name"),
                    resultSet.getString("billing_address"),
                    resultSet.getString("login"),
                    resultSet.getString("password"));

    @Override
    public void addNewUser(User user) {
        String sql = "INSERT INTO internet_auction.users VALUES (?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, user.getId(), user.getFull_name(),
                user.getBilling_address(), user.getLogin(), user.getPassword());
    }
}
