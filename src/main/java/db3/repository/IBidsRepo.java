package db3.repository;

import db3.entity.Bid;

import java.util.List;
import java.util.Map;

public interface IBidsRepo {

    List<Bid> getByUserID(int id);
    List<Map<String, Object>> maxBidsForEachLots();
    void deleteBidsByUserId(int userId);


}
