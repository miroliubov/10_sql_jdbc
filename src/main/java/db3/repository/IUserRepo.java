package db3.repository;

import db3.entity.User;

public interface IUserRepo {
    void addNewUser(User user);
}
